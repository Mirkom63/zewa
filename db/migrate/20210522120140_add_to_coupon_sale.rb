class AddToCouponSale < ActiveRecord::Migration[5.2]
  def change
    add_column :coupons, :sale, :string
  end
end
