class CreateCoupons < ActiveRecord::Migration[5.2]
  def change
    create_table :coupons do |t|
      t.boolean :used
      t.string :code
      t.string :hashcode
      t.timestamps
    end
  end
end
