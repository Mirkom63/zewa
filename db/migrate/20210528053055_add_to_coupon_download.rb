class AddToCouponDownload < ActiveRecord::Migration[5.2]
  def self.up
    change_table :coupons do |t|
      t.has_attached_file :download
    end
  end

  def self.down
    drop_attached_file :coupons, :download
  end
end
