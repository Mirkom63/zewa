class AddToUserDateWin < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :date_wins, :datetime
  end
end
