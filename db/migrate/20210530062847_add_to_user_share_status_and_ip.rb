class AddToUserShareStatusAndIp < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :fb_share, :boolean
    add_column :users, :vk_share, :boolean
    add_column :users, :ip, :string
  end
end
