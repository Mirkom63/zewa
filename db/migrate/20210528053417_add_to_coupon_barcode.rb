class AddToCouponBarcode < ActiveRecord::Migration[5.2]
  def self.up
    change_table :coupons do |t|
      t.has_attached_file :barcode
    end
  end

  def self.down
    drop_attached_file :coupons, :barcode
  end
end
