class AddToUserChangeSale < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :change_sale, :boolean
  end
end
