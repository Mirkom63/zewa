# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_06_15_082417) do

  create_table "coupons", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.boolean "used"
    t.string "code"
    t.string "sale"
    t.string "hashcode"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "coupon_file_name"
    t.string "coupon_content_type"
    t.bigint "coupon_file_size"
    t.datetime "coupon_updated_at"
    t.string "download_file_name"
    t.string "download_content_type"
    t.bigint "download_file_size"
    t.datetime "download_updated_at"
    t.string "barcode_file_name"
    t.string "barcode_content_type"
    t.bigint "barcode_file_size"
    t.datetime "barcode_updated_at"
  end

  create_table "users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.boolean "winner"
    t.integer "test_1"
    t.integer "test_2"
    t.integer "test_3"
    t.integer "test_4"
    t.integer "sale"
    t.boolean "change_sale"
    t.string "phone"
    t.string "code"
    t.datetime "time_send_code"
    t.bigint "coupon_id"
    t.datetime "get_coupon_datetime"
    t.string "email", default: "", null: false
    t.boolean "fb_share"
    t.boolean "vk_share"
    t.string "ip"
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "date_wins"
    t.datetime "date_share"
    t.index ["coupon_id"], name: "index_users_on_coupon_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "users", "coupons"
end
