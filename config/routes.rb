Rails.application.routes.draw do
  
  # require 'sidekiq/web'
  # mount Sidekiq::Web => '/sidekiq'
  
  
  
  devise_for :users
  
  get '/admin_fdb2d91c6b81f9d0ea5b6e094a1d1a6e' => 'admin#index'
  post '/admin/:type' => 'admin#ajax'
  
  get '/share' => 'index#share'
  
  get '/rules' => 'rules#index'
  get '/wins' => 'wins#index'
  get '/auth' => 'auth#index'
  get '/tests' => 'tests#index'
  get '/partners' => 'partners#index'
  get '/contacts' => 'contacts#index'
  get '/coupon' => 'coupon#index'
  get '/coupon/export' => 'coupon#export'
  
  get '/coupon/:type/:hash.png' => 'coupon#image'


  post '/tests/send_answer' => 'tests#send_answer'
  post '/tests/get_video' => 'tests#get_video'
  post '/tests/get_question' => 'tests#get_question'
  post '/auth/send_data'  => 'auth#send_data'
  post '/auth/send_code'  => 'auth#send_code'
  post '/auth/check_user' => 'auth#check_user'
  post '/auth/repeat' => 'auth#repeat'
  
  post '/coupon/check_social' => 'coupon#check_social'

  root 'index#index'
end
