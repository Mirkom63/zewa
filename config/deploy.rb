set :repo_url, 'git@bitbucket.org:Mirkom63/zewa.git'
set :application, 'zewa'
application = 'zewa'
set :rvm_type, :user
set :rvm_ruby_version, '2.6.3'
set :deploy_to, "/home/app/project/zewa/"


set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/assets public/system public/videos public/coupons}

set :linked_files, %w{config/secrets.yml config/database.yml config/boot.rb }


namespace :setup do
  desc 'Загрузка конфигурационных файлов на удаленный сервер'
  task :upload_config do
    on roles :all do
      execute :mkdir, "-p #{shared_path}"
      ['shared/config', 'shared/run'].each do |f|
        upload!(f, shared_path, recursive: true)
      end
    end
  end
end

namespace :nginx do
  desc 'Создание симлинка в /etc/nginx/conf.d на nginx.conf приложения'
  task :append_config do
    on roles :all do
      sudo :ln, "-fs #{shared_path}/config/nginx.conf /etc/nginx/conf.d/#{fetch(:application)}.conf"
    end
  end
  desc 'Релоад nginx'
  task :reload do
    on roles :all do
      sudo :service, :nginx, :reload
    end
  end
  desc 'Рестарт nginx'
  task :restart do
    on roles :all do
      sudo :service, :nginx, :restart
    end
  end
  after :append_config, :restart
end

namespace :application do
  desc 'Запуск Thin'
  task :start do
    on roles(:app) do

      execute "cd #{release_path} && ~/.rvm/bin/rvm default do bundle exec thin -C config/thin.yml -R config.ru -a 127.0.0.1 -p 8080 start  -s3 --socket /tmp/zewa.sock --pid /tmp/zewa.pid"

    end
  end
  desc 'Завершение Thin'
  task :stop do
    on roles(:app) do

      execute "if [ -f /tmp/zewa.0.pid ] && [ -e /proc/$(cat /tmp/zewa.0.pid) ]; then kill -9 `cat /tmp/zewa.0.pid`; fi"
      execute "if [ -f /tmp/zewa.1.pid ] && [ -e /proc/$(cat /tmp/zewa.1.pid) ]; then kill -9 `cat /tmp/zewa.1.pid`; fi"
      execute "if [ -f /tmp/zewa.2.pid ] && [ -e /proc/$(cat /tmp/zewa.2.pid) ]; then kill -9 `cat /tmp/zewa.2.pid`; fi"


      execute "cd #{release_path} && ~/.rvm/bin/rvm default do bundle exec rake db:migrate RAILS_ENV=production"
    end
  end
end


namespace :deploy do

ask(:message, "Commit message?")


  after :finishing, 'application:stop'
  after :finishing, 'application:start'
  after :finishing, :cleanup
end
