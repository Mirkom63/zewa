class ApplicationMailer < ActionMailer::Base
  default from: 'support@save-forest-with-zewa-i-5ka.ru'
  layout 'mailer'
end
