class SenderMailer < ApplicationMailer
  
  default from: 'support@save-forest-with-zewa-i-5ka.ru'
  
  
  def coupon(user_id)

    current_user=User.find(user_id)
    @coupon=current_user.coupon

    attachments['Купон на продукцию Zewa в "Пятёрочке".png'] = File.read("#{Rails.root}/public/coupons/#{@coupon.sale}/#{@coupon.hashcode}/main.png")
    
    mail(to: current_user.email, subject: 'Купон на продукцию Zewa в "Пятёрочке"')

  end
  
  
end
