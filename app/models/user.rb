class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  belongs_to :coupon
  
  def get_test_id


  	if self.test_1.blank?
  		@test_id='1'
  	else
  		if self.test_2.blank?
	  		@test_id='2'
	  	else
	  		if self.test_3.blank?
		  		@test_id='3'
		  	else
		  		if self.test_4.blank?
			  		@test_id='4'
			  	else
            if self.coupon_id.blank?
              @test_id='auth'
            else
              @test_id='coupon'
            end
            
          end
		  	end
	  	end
  	end


  end
end
