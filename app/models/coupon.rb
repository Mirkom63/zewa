class Coupon < ApplicationRecord
  has_many :user
  
  
  has_attached_file :coupon
	validates_attachment_content_type :coupon, content_type: /\Aimage\/.*\Z/
  
  has_attached_file :download
	validates_attachment_content_type :download, content_type: /\Aimage\/.*\Z/
  
  has_attached_file :barcode
	validates_attachment_content_type :barcode, content_type: /\Aimage\/.*\Z/
  
  
  def download_image
  end
  
  def converter
    
    self.convert_barcode
    
    sleep(0.1)
    
    self.coupon_image
    
    sleep(0.1)
    
    self.download_image
    
    sleep(0.1)
    
  end
  
  def download_image
    
    
    
    img = Magick::Image.read("#{Rails.root}/public/img/mail/download/#{self.sale}.png").first
    mark = Magick::Image.read("#{Rails.root}/public#{self.barcode.url(:original, timestamp: false)}").first
    mark.background_color = "Transparent"
  
    img1 = img.composite(mark, Magick::NorthWestGravity, 159,670, Magick::OverCompositeOp)
    
    watermark_text = Magick::Draw.new
    watermark_text.annotate(img1, 0,0,209,890, self.code) do
      self.pointsize = 30
    end
  
    img1.write("#{Rails.root}/tmp/download_#{self.id}.png")
    
    self.download=File.open("#{Rails.root}/tmp/download_#{self.id}.png")
    self.save(validate: false)
    
    FileUtils.rm_rf("#{Rails.root}/tmp/download_#{self.id}.png")
    
    
  end
  
  
  def coupon_image
    
    img = Magick::Image.read("#{Rails.root}/public/img/mail/coupon/#{self.sale}.png").first
    mark = Magick::Image.read("#{Rails.root}/public#{self.barcode.url(:original, timestamp: false)}").first
    mark.background_color = "Transparent"
  
    img1 = img.composite(mark, Magick::NorthWestGravity, 420,620, Magick::OverCompositeOp)
    
    watermark_text = Magick::Draw.new
    watermark_text.annotate(img1, 0,0,470,840, self.code) do
      self.pointsize = 30
    end
    
    
    img1.write("#{Rails.root}/tmp/coupon_#{self.id}.png")

    self.coupon=File.open("#{Rails.root}/tmp/coupon_#{self.id}.png")
    self.save(validate: false)
    
    FileUtils.rm_rf("#{Rails.root}/tmp/coupon_#{self.id}.png")
    
    
  end
  
  
  
  def convert_barcode
    
    require 'barby'
    require 'barby/barcode/code_128'
    
    require 'barby/outputter/png_outputter'
    
    barcode = Barby::Code128C.new(self.code)
    
    blob = Barby::PngOutputter.new(barcode).to_png( xdim: 3, margin: 0, height: 180)
    File.open("#{Rails.root}/tmp/barcode_#{self.id}.png", 'wb'){|f| f.write blob }
    
    self.barcode=File.open("#{Rails.root}/tmp/barcode_#{self.id}.png")
    self.save(validate: false)
    
    FileUtils.rm_rf("#{Rails.root}/tmp/barcode_#{self.id}.png")
    
    
    
  end
end
