// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery_ujs
//= require_tree .


$(document).ready(function(){
	$('input[type="tel"]').mask('+7 (000) 000-00-00');

	$(document).on('click','.js-open-menu',function(){
		$(this).removeClass('js-open-menu').addClass('js-close-menu');
		$('.mobile_menu').slideDown();
	});
	
	$(document).on('click','.js-close-menu',function(){
		$(this).removeClass('js-close-menu').addClass('js-open-menu');
		$('.mobile_menu').slideUp();
	});
	
	
	$(document).on('click','.ym-goal',function(){
		var goal=$(this).attr('goal');
		setTimeout(function(){
			ym(79962055,'reachGoal',goal);
		});
	});
});