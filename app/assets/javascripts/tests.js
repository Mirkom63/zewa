$(document).ready(function(){
  
  var timer_loader;
  
  $(document).on('click','.video',function(){
    if($('.button_play').css('display')=='block'){
      video=$('.video').get(0);
      video.play();
      setTimeout(function(){
        $('.button_play').fadeOut();
      });
    }else{
      video=$('.video').get(0);
      video.pause();
      setTimeout(function(){
        $('.button_play').fadeIn();
      });
    }
    
    
  });
  
  
  $(document).on('change','#rulles',function(){
    if($(this).prop('checked')==true){
      $('.form_phone button').prop('disabled',false);
    }else{
      $('.form_phone button').prop('disabled',true);
    }
  });
  
  $(document).on('click','.js-next-question',function(){
  
    $(this).removeClass('js-next-question');
    
    timer_loader=setTimeout(function(){
      $('.loader').show();
    },1000);
    
    $.ajax({
      type     :'POST',
      data: false,
      url  : '/tests/get_video',
      success  : function(response) {
        $('.video_container_append').html(response);
        initialize_video();

        $.ajax({
          type     :'POST',
          data: false,
          url  : '/tests/get_question',
          success  : function(response) {
            clearTimeout(timer_loader);
            $('.loader').hide();
            $('.test_container').html(response);
          }
        });

      }
    });

  });

  $(document).on('click','.js-set-answer',function(){
    
    $(this).removeClass('js-set-answer');
    
    timer_loader=setTimeout(function(){
      $('.loader').show();
    },1000);
    
    
    $.ajax({
      type     :'POST',
      data: {
        answer_id: $(this).attr('answer_id'),
      },
      url  : '/tests/send_answer',
      success  : function(response) {
        clearTimeout(timer_loader);
        $('.loader').hide();
        $('.test_container').html(response);
      }
    });
  });
  
  

  var timer_play;
  function initialize_video(){
    video=$('.video').get(0);
    
    $('.test_container').hide();
    $('.video_container_loader').show();
    $('.video_container_skip').show();
    
    timer_play=setTimeout(function(){
      $('.button_play').show();
    },1000);
    
    $('.video').on(
    "timeupdate",
    function(event){
      clearTimeout(timer_play);
      if($('.button_play').css('display')=='block'){
        console.log('hide');
        $('.button_play').fadeOut();
      }
      
      
      var percent_video=(this.currentTime/video.duration)*100;
      $('.video_container_loader_line').width(percent_video+'%');
    
      if(percent_video>=100){
        $('.test_container').show();
        $('.video').remove();
        $('.video_loop').show();
        $('.video_container_loader').hide();
        $('.video_container_skip').hide();

        video=$('.video_loop').get(0);
        video.play();

      }
    });
  }

  initialize_video();
  
 
  
  $('.video_container_skip').click(function(){

    var video=$('.video').get(0);
    video.currentTime=video.duration;


  });
    
});