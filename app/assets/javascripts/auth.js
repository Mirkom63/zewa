$(document).ready(function(){
		
	$('input[type="checkbox"]').checkboxradio();
	
	$('.js-code-back').click(function(){		
		$('.block_send_code').hide();
		$('.block_send_phone').show();
	});

	$('input').keyup(function(){
		$(this).removeClass('empty_input');
	});
	
	$(document).on('click','.js-resend-code',function(){
		
		timer_loader=setTimeout(function(){
      $('.loader').show();
    },1000);
		
		$.ajax({
			type     :'POST',
			data: form.serialize(),
			url  : '/auth/send_data',
			success  : function(response) {
				
				clearTimeout(timer_loader);
				$('.loader').hide();
				
				$('.block_send_code').show();
				$('.block_send_phone').hide();
				code_timer(60);
				
				setTimeout(function(){
					ym(79962055,'reachGoal','form_code');
				});
				
			}
		});
	});
	
	var timer_time;
	function code_timer(time){
		time--;
		if(time<10){
			time_text='0'+time;
		}else{
			time_text=time;
		}
		$('.code_resend').text('Выслать код заново: 00:'+time_text);
		timer_time=setTimeout(function(){
			if(time==0){
				$('.code_resend').html('<a href="javacsript:void(0)" class="js-resend-code">Выслать код заново</<a>');
			}else{
				code_timer(time);
			}
			
		},1000);
	}

	$('.form_phone').submit(function(e){
		e.preventDefault();
		
		form=$(this);

		var send_form=true;

		$(this).find('input').each(function(){
			if($(this).val().length==0){
				send_form=false;
				$(this).addClass('empty_input');
			}
		});
		
		$('.form_error').hide();

		if(send_form==true && $('#rulles').prop('checked')==true){
			
			timer_loader=setTimeout(function(){
	      $('.loader').show();
	    },1000);
			
			$.ajax({
				type     :'POST',
				data: form.serialize(),
				url  : '/auth/check_user',
				success  : function(response) {
					if(response['have_with_phone']==true){
						
						clearTimeout(timer_loader);
						$('.loader').hide();
						
						$('.form_error_phone').show();
					}
					if(response['have_with_email']==true){
						
						clearTimeout(timer_loader);
						$('.loader').hide();
						
						$('.form_error_email').show();
					}
					
					if(response['have_with_phone']==false && response['have_with_email']==false){
						$('.form_phone_error').hide();
						
						$.ajax({
		          type     :'POST',
		          data: form.serialize(),
		          url  : '/auth/send_data',
		          success  : function(response) {
								
								clearTimeout(timer_loader);
		            $('.loader').hide();
								
	            	$('.block_send_code').show();
								$('.block_send_phone').hide();
								
								clearTimeout(timer_time);
								code_timer(60);
								
								setTimeout(function(){
									ym(79962055,'reachGoal','form_main');
								});
					
	          	}
		        });
						
					}else{
						$('.form_phone_error').show();
					}
					
									
				}
			});
			
		}

		

	});


	$('.form_code').submit(function(e){
		e.preventDefault();

		var send_form=true;

		$(this).find('input').each(function(){
			if($(this).val().length==0){
				send_form=false;
				$(this).addClass('empty_input');
			}
		});

		if(send_form==true){
			
			timer_loader=setTimeout(function(){
	      $('.loader').show();
	    },1000);
			
			$.ajax({
        type     :'POST',
        data: $(this).serialize(),
        url  : '/auth/send_code',
        success  : function(response) {
					
					
					clearTimeout(timer_loader);
					$('.loader').hide();
					
					if(response=='no'){
						$('.form_error_code').show();
					}else{
						window.location='/coupon';
					}
          
        }
      });
		}

		

	});
	
});