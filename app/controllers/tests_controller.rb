class TestsController < ApplicationController


	def get_video
		render 'tests/video', layout: nil
	end
	def get_question
		render 'tests/question', layout: nil
	end

	def send_answer
			
		@answer=true

		if current_user.get_test_id=='1'
			if params[:answer_id]=='2'
				@answer=false
			end
		end
		if current_user.get_test_id=='2'
			if params[:answer_id]=='1'
				@answer=false
			end
		end
		if current_user.get_test_id=='3'
			if params[:answer_id]=='1'
				@answer=false
			end
		end
		if current_user.get_test_id=='4'
			@answer=false
			if params[:answer_id]=='2'
				@answer=true
			end
		end

		if @answer==true
			if current_user.sale==0
				current_user.sale=10
			end
			current_user.sale=current_user.sale+10
		end
		current_user["test_#{current_user.get_test_id}"]=params[:answer_id]
		current_user.save(validate: false)

		render 'tests/answer', layout: nil

	end

	def index
		
		if current_user.get_test_id=='auth'
	  		redirect_to '/auth'
	  	end
	  	if current_user.get_test_id=='coupon'
	  		redirect_to '/coupon'
	  	end
	end

end
