class AdminController < ApplicationController
  def index
    # if current_user.email=='a.petrov@pervichka.pro' or current_user.email=='89601091902@mail.ru' or current_user.email=='dashulka23@gmail.com'
    # 
    # else
    #   redirect_to '/'
    # end
  end
  
  def ajax
    
    
    if params[:type]=='delete_user'
      user=User.find(params[:user_id])
      user.destroy
      
      redirect_to '/admin_fdb2d91c6b81f9d0ea5b6e094a1d1a6e/'
    end
    
    if params[:type]=='find_user'
      
      coupon=Coupon.find_by(code: params[:code])
      if coupon.present?
        user=coupon.user.first
      else
        user=User.find_by(phone: params[:phone])
        if user.blank?
          user=User.find_by(email: params[:email])
        end
      end
      
      if user.present?
        redirect_to '/admin_fdb2d91c6b81f9d0ea5b6e094a1d1a6e/?user_id='+user.id.to_s
      else
        redirect_to '/admin_fdb2d91c6b81f9d0ea5b6e094a1d1a6e/?user_id=false'
      end
      
    end
    if params[:type]=='get_coupon'
      
      coupon=Coupon.where(used: false, sale: params[:sale])
			if coupon.blank?
				coupon=Coupon.where(used: false).order(sale: :desc)
			end
      
      coupon=coupon.first
      
      coupon.used=true
      coupon.save(validate: false)
      
      redirect_to '/admin_fdb2d91c6b81f9d0ea5b6e094a1d1a6e/?hashcode='+coupon.hashcode
    end
    
  end
end
