class ApplicationController < ActionController::Base
  
  
  before_action :check_auth?
  after_action :allow_yandex_iframe
  
  
  def allow_yandex_iframe
    response.headers['X-Frame-Options'] = 'ALLOW-FROM https://metrika.yandex.ru'
  end

	def check_auth?
    
    @client = DeviceDetector.new(request.user_agent)
    if @client.device_type=='smartphone' or @client.device_type=='phablet'
      @mobile=true
    else
      @mobile=false
    end
    
    if params[:action]!='share'
      if not user_signed_in?
        
        hash=md5 = Digest::MD5.new
        hash=md5.hexdigest Time.new.to_s
        
        new_user=User.new
        new_user.email="#{hash}@example.com"
        new_user.sale=0
        new_user.ip=request.ip
        new_user.save!(validate: false)

        sign_in_and_redirect new_user, :event => :authentication 

      end
    end
	end


	def after_sign_in_path_for(resource)
    if params[:redirect_to_test].present?
      '/tests'    
    else
      '/'    
    end
    
	end
  
  
end
