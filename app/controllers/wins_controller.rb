class WinsController < ApplicationController
  def index
    if User.where(winner: true).blank?
      redirect_to '/'
    end
  end
end
